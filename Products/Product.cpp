#include "Product.h"
#include<string>



Product::Product(int newId, std::string newName, float newRawPrice)
{
	m_id = newId;
	m_name = newName;
	m_rawPrice = newRawPrice;
}

Product::~Product()
{
}

int Product::getId() const
{
	return m_id;
}

std::string Product::getName() const
{
	return m_name;
}

float Product::getRawPrice() const
{
	return m_rawPrice;
}
