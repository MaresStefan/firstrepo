#pragma once
#include"IPriceable.h"
#include<string>
class Product : public IPriceable
{
public:
	Product(int newId, std::string newName, float newRawPrice);
	~Product();
	int getId() const;
	std::string getName() const;
	float getRawPrice() const;
protected:
	int m_id;
	std::string m_name;
	float m_rawPrice;

};

