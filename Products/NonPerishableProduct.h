#pragma once
#include"Product.h"
#include<string>
class NonPerishableProduct : public Product
{
public:
	enum class Type
	{
		Clothing,
		SmallAppliances,
		PersonalHygiene
	};
	NonPerishableProduct(int id, std::string name, float rawPrice, Type type);
	int getVAT() const override;
	float getPrice() const override;

	friend std::ostream& operator<<(std::ostream& os, const NonPerishableProduct& prod);
	~NonPerishableProduct();
private:
	Type m_type;
};

