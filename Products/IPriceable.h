#pragma once
class IPriceable
{
public:
	virtual ~IPriceable() = default;
	int virtual getVAT() = 0;
	float virtual getPrice() = 0;
};

